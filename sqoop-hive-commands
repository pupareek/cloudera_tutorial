###########################################################
#Copy data from MySQL to local file system
##################################################################

mysql -u root -p 
update mysql.user set file_priv = 'Y' where user = 'retail_dba';
commit;
exit;


##### On terminal prompt, run

service mysqld restart
mysql -u retail_dba -p 
use retail_db;

########Lets first check the tables structure,data in table and file

select * from customers into outfile '/tmp/customers.psv' fields terminated by '|' lines terminated by '\n';

more /tmp/customers.psv



#########Launch hive

hive
CREATE DATABASE retail_stg;
use retail_stg;

###############################################################
#create hive managed table
################################################################

CREATE TABLE customers (
customer_id       int,
customer_fname    string,
customer_lname    string,
customer_email    string,
customer_password string,
customer_street   string,
customer_city     string,
customer_state    string,
customer_zipcode  string 
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS TEXTFILE;

select * from customers limit 10;

load data local inpath '/tmp/customers.psv' overwrite into table customers;

select * from customers limit 10;

describe customers;

describe formatted customers;

hdfs -ls /user/hive/warehouse/retail_stg.db/customers

###############################################################
#Load data from HDFS to hive table
################################################################
#Prepare HDFS stage directory
################################################################

hadoop fs -mkdir /user/root/customers
hadoop fs -put /mysqldata/customers.psv /user/root/customers
hadoop fs -ls /user/root/customers


#Launch hive
hive
use retail_stg;


###############################################################
#create external table customer in hive
################################################################

CREATE EXTERNAL TABLE customers_ext (
customer_id       int,
customer_fname    string,
customer_lname    string,
customer_email    string,
customer_password string,
customer_street   string,
customer_city     string,
customer_state    string,
customer_zipcode  string 
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS TEXTFILE
LOCATION 'hdfs:///user/root/customers/';


load data inpath '/user/root/customers/*' overwrite into table customers;
hadoop fs -ls /user/root/customers
#######No file will be listed

hdfs dfs -mkdir /user/hive/warehouse/retail.db

###############################################################
#Import orders and order_items table from mysql to hive database
################################################################

sqoop import \
  --connect "jdbc:mysql://quickstart.cloudera:3306/retail_db" \
  --username=retail_dba \
  --password=cloudera \
  --table order_items \
  --fields-terminated-by '|' \
  --lines-terminated-by '\n' \
  --hive-home /user/hive/warehouse/retail_stg.db \
  --hive-import \
  --hive-overwrite \
  --hive-table retail_stg.order_items \
  --outdir java_files


sqoop import \
  --connect "jdbc:mysql://quickstart.cloudera:3306/retail_db" \
  --username=retail_dba \
  --password=cloudera \
  --table orders \
  --fields-terminated-by '|' \
  --lines-terminated-by '\n' \
  --hive-home /user/hive/warehouse/retail_stg.db \
  --hive-import \
  --hive-overwrite \
  --hive-table retail_stg.orders \
  --outdir java_files

show tables;

describe formatted order_items;
hdfs dfs -ls /user/hive/warehouse/retail_stg.db/order_items/
hdfs dfs -cat /user/hive/warehouse/retail_stg.db/order_items/part-m-00000  | head


################################
#Lets Switch to hue
################################
########################################
#Create partition table
#######################################

CREATE TABLE order_items_part (
order_item_id int,
order_item_order_id int,
order_item_order_date string,
order_item_product_id int,
order_item_quantity smallint,
order_item_subtotal float,
order_item_product_price float
)
PARTITIONED BY (order_month string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS TEXTFILE;

########################################
#Adding partition manually
#######################################

alter table order_items_part add partition (order_month='2014-07');

########################################
#Insert data into partition table
#######################################

insert into table order_items_part partition (order_month='2014-07')
select
oi.order_item_id,
oi.order_item_order_id ,
o.order_date as order_item_order_date,
oi.order_item_product_id ,
oi.order_item_quantity ,
oi.order_item_subtotal ,
oi.order_item_product_price
from order_items oi inner join orders o
on o.order_id=oi.order_item_order_id
where substr(o.order_date, 1, 7) = '2014-01';

########################################
#Create table with bucket in partition
#######################################


CREATE TABLE order_items_bucket (
order_item_id int,
order_item_order_id int,
order_item_order_date string,
order_item_product_id int,
order_item_quantity smallint,
order_item_subtotal float,
order_item_product_price float
)
PARTITIONED BY (order_month string)
CLUSTERED BY (order_item_order_id) INTO 4 BUCKETS
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS TEXTFILE;


########################################
#Dynamic partition creation
#######################################


set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.enforce.bucketing = true;

insert into table order_items_bucket partition (order_month)
select
oi.order_item_id,
oi.order_item_order_id ,
o.order_date as order_item_order_date,
oi.order_item_product_id ,
oi.order_item_quantity ,
oi.order_item_subtotal ,
oi.order_item_product_price,
substr(o.order_date, 1, 7) order_month
from order_items oi inner join orders o
on o.order_id=oi.order_item_order_id;

#####################################################
#Collecting stats on table to improve performance
#####################################################

Use retail_stg;
select
oi.order_item_id,
oi.order_item_order_id ,
o.order_date as order_item_order_date,
oi.order_item_product_id ,
oi.order_item_quantity ,
oi.order_item_subtotal ,
oi.order_item_product_price
from order_items oi inner join orders o
on o.order_id=oi.order_item_order_id
where substr(o.order_date, 1, 7) = '2014-01';

Analyze table order_items compute statistics;
Analyze table order_items compute statistics for columns;

Analyze table orders compute statistics;
Analyze table orders compute statistics for columns;

select
oi.order_item_id,
oi.order_item_order_id ,
o.order_date as order_item_order_date,
oi.order_item_product_id ,
oi.order_item_quantity ,
oi.order_item_subtotal ,
oi.order_item_product_price
from order_items oi inner join orders o
on o.order_id=oi.order_item_order_id
where substr(o.order_date, 1, 7) = '2014-01';



###############################
#Running Hive queries using file
################################

mkdir hqls
vi insert_cust_ca.hql

use retail_stg;
drop table if exists cust_ctas;
create table cust_ca as select * from customers where customer_state='CA' ;


#############################
#Sqoop Import -- Getting delta (--where)
##############################

sqoop import \
  --connect "jdbc:mysql://quickstart.cloudera:3306/retail_db" \
  --m 3 \
  --username=retail_dba \
  --password=cloudera \
  --table categories \
  --target-dir /user/hive/warehouse/retail_stg.db/categories \
  --append \
  --fields-terminated-by '|' \
  --lines-terminated-by '\n' \
  --split-by category_id \
  --where "category_department_id =2" \
  --outdir java_files


CREATE TABLE categories (
category_id int,
category_department_id int,
category_name string
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS TEXTFILE
LOCATION 'hdfs:///user/hive/warehouse/retail_stg.db/categories';

Select * from categories;



#######################
#Sqoop Export
#######################

--Connect to mysql and create database for reporting database
--user:root, password:cloudera
mysql -u retail_dba -p
use retail_db;
create table catg_dep_02 as select * from retail_db.categories where 1=2;
exit;

sqoop export --connect "jdbc:mysql://quickstart.cloudera:3306/retail_db" \
       --username retail_dba \
       --password cloudera \
       --table catg_dep_02 \
       --export-dir /user/hive/warehouse/retail_stg.db/categories \
       --input-fields-terminated-by '|' \
       --input-lines-terminated-by '\n' \
       --num-mappers 2 \
       --batch \
       --outdir java_files


#######################################################################
#Run below drop table command
#On dropping managed table the underlying directory data is also deleted
#####################################################################

drop table retail_stg.categories;






